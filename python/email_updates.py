#!/usr/bin/env python3
"""
Send updates about the status of the door by sms or e-mail.
"""

import urllib.request
import urllib.parse
import smtplib
import logging
from email.message import EmailMessage
import sqlite3
from sqlitedict import SqliteDict
import argparse

ADDRESS = 5  # Lora address
MSG_RECENCY_LIMIT = 60 * 60  # 1h in seconds
BATTERY_LOW_VOLTAGE = 11.5
SMS_URL = "https://smsapi.free-mobile.fr/sendmsg"
SMS_USER = 0000
SMS_KEY = ''
ERROR_EML = 'gabe@squirrelsoup.net'


def get_message_recency(c):
    c.execute("""   SELECT datetime(Timestamp, 'unixepoch'),
                        strftime('%s', 'now') - Timestamp AS time_delta,
                        Key, Value
                    FROM events
                    WHERE key LIKE "{}-%"
                    ORDER BY Timestamp DESC LIMIT 1;
            """.format(ADDRESS))
    row = c.fetchone()
    return row[1]


def get_latest_value(c, key):
    c.execute("""   SELECT datetime(Timestamp, 'unixepoch'),
                        strftime('%s', 'now') - Timestamp AS time_delta,
                        Key, Value
                    FROM events
                    WHERE key="{}-{}"
                    ORDER BY Timestamp DESC LIMIT 1;
            """.format(ADDRESS, key))
    row = c.fetchone()
    return row[3]


def get_state_change(c):
    c.execute("""   SELECT datetime(Timestamp, 'unixepoch'),
                        strftime('%s', 'now') - Timestamp AS time_delta,
                        Key, Value, Timestamp
                    FROM events
                    WHERE key="{}-{}"
                    ORDER BY Timestamp DESC LIMIT 2;
            """.format(ADDRESS, 'D'))
    results = c.fetchall()
    if results[0][3] != results[1][3]:
        return True, results[0][3], results[0][4]
    else:
        return False, results[0][3], results[0][4]


def send_email(msg):
    eml = EmailMessage()
    eml.set_content(msg)
    eml['Subject'] = msg
    eml['From'] = 'cotcot@neyramand.fr'
    eml['To'] = ERROR_EML
    s = smtplib.SMTP('neyramand.fr')
    logging.info("Sending e-mail: {}".format(eml))
    s.send_message(eml)
    s.quit()


def send_sms(msg):
    logging.info("Sending SMS: {}".format(msg))
    query = "{}?user={}&pass={}&msg={}".format(SMS_URL, SMS_USER, SMS_KEY,
                                               urllib.parse.quote(msg))
    with urllib.request.urlopen(query) as f:
        return f.status
    return None


def main(args):
    conn = sqlite3.connect(args.db_name)
    c = conn.cursor()
    l_dict = SqliteDict(args.db_name, autocommit=True)

    # Look for a door state change, send text message if successful
    state_change, door_state, timestamp = get_state_change(c)
    try:
        last_state_change = l_dict['last_state_change']
    except KeyError:
        last_state_change = 0
    if state_change and timestamp > last_state_change:
        logging.info(timestamp)
        l_dict['last_state_change'] = timestamp
        msg = "Cot cot! New door state: {}".format(door_state)
        send_sms(msg)

    if args.do_errors:
        # Check that the last message is < 2h otherwise send e-mail/text
        message_recency = get_message_recency(c)
        msg = None
        if message_recency > MSG_RECENCY_LIMIT:
            msg = ("Cot cot. Pas de nouvelles depuis {} minutes -- "
                   "il y'a surement un Cot. problème. "
                   "Il faut aller verifier que tout fonctionne."
                   "Cot cot.").format(int(message_recency/60))
            send_email(msg)
            send_sms(msg)
        # Check for current error status, send e-mail/text if needed
        str_error = get_latest_value(c, 'E')
        if str_error != "NONE":
            msg = ("Cot cot. Erreur: {}. "
                   "Réparer et redémarrer en rebranchant la batterie. "
                   "Cot cot.").format(str_error)
            send_email(msg)
            send_sms(msg)
        if get_latest_value(c, 'D') == 'UNKNOWN':
            msg = ("Cot cot. La porte est dans un état incertain, "
                   "il faudrait la verifier. Cot. puis redémarrer en "
                   "re-branchant la batterie. Cot cot.")
            send_email(msg)
            send_sms(msg)
        # Check battery voltage and send e-mail/text if recharge needed
        voltage = get_latest_value(c, 'V')
        if voltage < BATTERY_LOW_VOLTAGE:
            msg = ("Cot cot. La batterie est faible, il faudrait la recharger. "
                   "Tension actuelle: Cot. {}V. Cot cot.").format(voltage)
            send_email(msg)
            send_sms(msg)
    conn.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Send information and error updates regarding the coop.')
    parser.add_argument('--do_errors', dest='do_errors', action='store_true')
    parser.add_argument('--db_name', dest='db_name')
    parser.add_argument('--sms_user', dest='sms_user')
    parser.add_argument('--sms_pass', dest='sms_key')
    SMS_USER = args.sms_user
    SMS_KEY = args.sms_key
    args = parser.parse_args()
    main(args)
