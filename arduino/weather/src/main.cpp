      // Feather9x_TX
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messaging client (transmitter)
// with the RH_RF95 class. RH_RF95 class does not provide for addressing or
// reliability, so you should only use RH_RF95 if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example Feather9x_RX
// #include <Arduino.h>
#include <SPI.h>
// #include <interrupt.h>
#include <RH_RF95.h>

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3

#define BOARD_ADDR 8
#define MASTER_ADDR 1
#define SENSOR_AN_PIN 0


// Change to 434.0 or other frequency, must match RX's freq!
// #define RF95_FREQ 868.1
#define RF95_FREQ 868.1
#define DATA_PIN A0

uint8_t message_buf[80];

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);
DHT dht(DATA_PIN, DHT22);

void setup() 
{
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  // while (!Serial);
  Serial.begin(9600);
  delay(100);

  Serial.println("Feather LoRa TX Test!");

  // manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    Serial.println("LoRa radio init failed");
    while (1);
  }
  Serial.println("LoRa radio init OK!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
  }
  Serial.print("Set Freq to: "); Serial.println(RF95_FREQ);
  
  // Defaults after init are 868.1MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then 
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);

  rf95.setThisAddress(BOARD_ADDR);
  rf95.setHeaderTo(MASTER_ADDR);
  rf95.setHeaderFrom(BOARD_ADDR);

  dht.begin();
}

int16_t packetnum = 0;  // packet counter, we increment per xmission
uint8_t r_size = 0;
float temp = 0;
float humidity = 0;

void loop()
{
  Serial.println("Sending to rf95_server");
  rf95.setHeaderId(packetnum++);

  temp = dht.readTemperature(false);
  humidity = dht.readHumidity();
  String str_temp = String(temp);
  String str_humidity = String(humidity);
  r_size = sprintf((char*)message_buf, "T=%s;H=%s;", str_temp.c_str(), str_humidity.c_str());
  
  Serial.print("Sending...");
  Serial.println((char*)message_buf);
  delay(10);
  rf95.send((uint8_t *)message_buf, r_size);
  Serial.println("Waiting for packet to complete..."); delay(10);
  rf95.waitPacketSent();

  // Log every 5m
  delay(1000 * 60 * 5);
}
