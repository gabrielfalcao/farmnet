# FarmNet

This project consists of a centralized server for receiving & storing LoRA based messages on a Raspberry PI, as well as a series of Arduino-based firmwares for various nodes.  Currently there are two functional nodes: a chicken-coop door manager, as well as a simple weather station (Temp/Humidity).

Currently the other arduino nodes are in partial states of completion, but may also inspire new projects.  Contributions are welcome.

---

En français:

Ce projet est composé d'un serveur central pour recevoir et stocquer des méssages LoRA sur un Raspberry Pi, ainsi qu'un ensemble de scripts Arduino qui communiquent avec le serveur.  Il y'a actuellement un programme pour gérér l'ouverture d'un poulailler ainsi qu'une simple station meteo qui renvoie la temperature et l'humiditée.

Actuellement les nodes arduino 'chickens' et 'weather' sont les seuls complets dans leur developpement.

P.S. This project is not related to 360 FarmNet or the FarmNet vehicle management Android App.  After a too many mental cycles spent on finding a better name I just went with it.

## Toolkit
The arduino code is in [platformio](https://platformio.org) projecs, so I suggest installing the platformio commandline tools:

`pip install -U platformio`

You may need to install the missing libraries using `pio lib install`.  Depending on which libs are missing you can search around for them and then install them.

Compiling and uploading the firmware is as simple as:

`platformio run -e feather -t upload`

## Hardware
This current version works with a Raspi using the Dragino Lora/GPS shield (I don't use the GPS part for anything but it could be used for timekeeping).  You can find more information about the shield [here](http://wiki.dragino.com/index.php?title=Use_Lora/GPS_HAT_%2B_RaspberryPi_to_set_up_a_Lora_Node).  Make sure you re-solder the pins as per the instructions on the wiki page.  Other LoRA shields *should* work out of the box, but are not tested.

I use Adafruit Lora Feathers, which are a great board with a lot of capability.  You can get some [here](https://www.adafruit.com/product/3078).

## Running Server
Get the latest version of the rf95 library here and either put it in the python folder or make it accessible on the python path: 
* https://github.com/ladecadence/pyRF95

If you want colored log statements, install coloredlogs

To run, simply run:

`python FarmNet.py --log DEBUG`

## Monitoring

An example monitoring script is in python/email_updates.py.  It is capable of sending e-mails as well as SMS messages through the Free SMS portal by passing the `--sms_key` and `--sms_pass` parameters.
I run this script using a user-based crontab with read access to the database:

`  0 *  *   *   *     /home/gabriel/Src/FarmNet/python/email_updates.py --do_errors --db_name /home/gabriel/Src/FarmNet/python/farmnet.sqlite3` --sms_user DDDDD --sms_key XXXXX